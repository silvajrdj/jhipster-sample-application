/**
 * Data Access Objects used by WebSocket services.
 */
package me.entregge.api.web.websocket.dto;
