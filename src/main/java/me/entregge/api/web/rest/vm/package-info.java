/**
 * View Models used by Spring MVC REST controllers.
 */
package me.entregge.api.web.rest.vm;
