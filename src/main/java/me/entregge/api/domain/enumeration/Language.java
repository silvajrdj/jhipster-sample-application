package me.entregge.api.domain.enumeration;

/**
 * The Language enumeration.
 */
public enum Language {
    FRENCH, ENGLISH, SPANISH
}
